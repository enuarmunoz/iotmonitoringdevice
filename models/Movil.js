'use strict'
const mongoose = require('mongoose')

const Schema = new mongoose.Schema({
    Usuario:{
        type:String, 
        required: [true, 'El nombre es obligatorio']
    },
    Usuario_Id:{
        type:String,
        required:[true, 'Es necesario que ingrese el ID del USUARIO']
    },
    EstadoUsuario:{
        type: Boolean,
        default:true,
        required: [true, 'El estado de el usuario es necesario']
    },
    Imei:{
        type: String,
        required:[true, 'El Email del celular es obligatorio']
    },
    GpsEnabled:{
        type:Boolean,
        default: false
    },
    OpenAplicacion:{
        type:Boolean,
        default: false
    },
    Total_Cables_Pend:{
        type:Number
    },
    Total_Equipos_Pend:{
        type:Number
    },
    Total_Fotos_Pend:{
        type:Number
    },
    Total_Novedades_Pend:{
        type:Number
    },
    Total_Postes_Pend:{
        type:Number
    },
    Version_App:{
        type: String,
        required:[true, 'Es obligatoria la version de la aplicacion']
    },
    CurrentModuloApp:{
        type:String,
    },
    date_creation:{
        type: Date,
        default: Date.now
    },
    date_update:{
        type: Date,
        default: Date.now
    },
    Socket_id: {
        type: String,
        required:[true, 'ES Obligatorio el Id del socket']
    }
})

      module.exports = mongoose.model('MovilInteredes', Schema)