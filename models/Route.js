'use strict'
const mongoose = require('mongoose')

const Schema = new mongoose.Schema({
    id_Movil: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'movils',
        required: [true, 'El Email del celular es obligatorio']
    },
    Imei: {
        type: String,
        required: [true, 'El Email del celular es obligatorio']
    },
    Longitud: {
        type: Number,
    },
    Latitud: {
        type: Number,
    },
    DireccionAproximada: {
        type: String,
    },
    Ubicacion: {
        type: String,
    },
    date_creation: {
        type: Date,
        default: Date.now
    },
})

module.exports = mongoose.model('route', Schema)