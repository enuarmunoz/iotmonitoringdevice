#Rutas para la administracion de datos Socket


para instalar este software, descargue cada uno de los modulos y ejecute la siguiente instruccion 

```javascript
    npm i
```


para ejecutar este software, ejecute la siguiente instruccion

```javascript
    npm run start:dev
```



## Administrador De Movildes



###Crear una cuenta Movil

| URL | http://127.0.0.1:3000/v1/movil/create |
| -- | -- |
| Método | POST |
| Parámetros |  |

### Detallar todos los Moviles

| URL | http://127.0.0.1:3000/v1/movil//select/:id |
| -- | -- |
| Método | GET |

### Editar un movil

| URL | http://127.0.0.1:3000/v1/movil/update/:id |
| -- | -- |
| Método | POST |
| parámetros | |

### Seleccionar todos los Moviles

| URL | http://127.0.0.1:3000/v1/movil/selectAll |
| -- | -- |
| Método | POST |
| Parámetros | |




## Administrador De Rutas



###Ruta para una nueva conexion

| URL | http://127.0.0.1:3000/v1/router/newConect |
| -- | -- |
| Método | POST |
| Descripcion | guarda la nueva conexion del usuario y modifica el estado de el movil a true |
| Parámetros | |

### Ruta para una desconexion

| URL | http://127.0.0.1:3000/v1/router/newDisconect |
| -- | -- |
| Método | POST |
| Descripcion | guarda los datos de la ultima ubicacion y modifica el estado de el movil a false |
| Descripcion | |

### Guardar Rutas en tiempo real

| URL | http://127.0.0.1:3000/v1/router/saveRoute |
| -- | -- |
| Método | POST |
| Descripcion | guarda los datos de la ultima ubicacion |
| parámetros | |

### Seleccionar rutas por dias

| URL | http://127.0.0.1:3000/v1/router/selectRouteUser/:Imei/:initDate/:finishDate |
| -- | -- |
| Método | GET |
| Descripcion | va a traer todos las rutas de todos los usuarios que se realizaron en la fecha que parametricemos |
| Parámetros | Imei, Version_App, CurrentModuloApp, Usuario, Usuario_Id,  EstadoUsuario, Longitud, Latitud, DireccionAproximada |

### Seleccionar rutas por dias

| URL | http://127.0.0.1:3000/v1/router/selectLastRouteAllUser |
| -- | -- |
| Método | GET |
| Descripcion | se selecciona la ultima coneccion de todos los usuarios |
| Parámetros | Imei, Version_App, CurrentModuloApp, Usuario, Usuario_Id,  EstadoUsuario, Longitud, Latitud, DireccionAproximada |


### Seleccionar rutas por dias

| URL | http://127.0.0.1:3000/v1/router/selectAllUserDay/:DateInit/:DateFinish |
| -- | -- |
| Método | GET |
| Descripcion | se selecciona la ultima coneccion de todos los usuarios por fecha |
| Parámetros | Imei, Version_App, CurrentModuloApp, Usuario, Usuario_Id,  EstadoUsuario, Longitud, Latitud, DireccionAproximada |
