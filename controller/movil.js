'use strict'
const MovilesLibs = require('../libs/movilLibs')

async function create(data){
    const save = await MovilesLibs().create(data)
    return save
}

async function listAll(idUser){ 
    const listAll = await MovilesLibs().listAll(idUser)  
    return listAll
}

async function modify(id, data){
    const update = await MovilesLibs().update(id, data)
    return update
} 

async function list(id){
    const update = await MovilesLibs().listId(id)
    return update
} 

module.exports = { 
    create,
    listAll, 
    modify,
    list,
}