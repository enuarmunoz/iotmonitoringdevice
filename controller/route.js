'use strict'
const RouteLibs = require('../libs/routeLibs')
const MovilLibs = require('../libs/movilLibs')

async function optionModifyMovil(dataConect) {
    const dataUser = await MovilLibs().listId(dataConect.Imei)
    dataConect['id_Movil'] = `${dataUser[0]._id}`
    const SaveRoute = await RouteLibs().create(dataConect)
    return SaveRoute
}

/* ok */
async function newConectUser(dataConect, Estado) {
    const verifyMovil = await MovilLibs().listId(dataConect.Imei)
    if (verifyMovil[0] !== undefined) {
        dataConect['EstadoUsuario'] = Estado
        const updateMovile = await MovilLibs().UpdateEstado(dataConect.Imei, dataConect)
        if (updateMovile) {
            optionModifyMovil(dataConect)
        }
    }
    else {
        const createMovil = await MovilLibs().create(dataConect)
        if (createMovil) {
            optionModifyMovil(dataConect)
        }
    }
}

async function newDisconect(id, Estado) {
    const selectImei = await MovilLibs().listByIdSocket(id)
    if(selectImei[0] !== undefined){
    selectImei[0]['EstadoUsuario'] = Estado
    await MovilLibs().UpdateEstado(selectImei[0].Imei, selectImei[0])
    return selectImei[0].Imei
    } 
}

/* ok */
async function saveRoute(dataRoute) {
    const dataUser = await MovilLibs().listId(dataRoute.Imei)
    dataRoute['id_Movil'] = `${dataUser[0]._id}`
    console.log('DATAAAA',dataRoute)
    const data = await MovilLibs().UpdateEstado(dataRoute.Imei, dataRoute)
    console.log('validacion',data)
    if(dataRoute.Longitud == 0 || dataRoute.Longitud == 0.0){
        return true
    }else{
        await RouteLibs().create(dataRoute)
        return true
    }
}

/* OK */
async function listRouteUser(Imei, initial, finish) {
    const newDate = new Date(initial)
    const finishDate = new Date(finish)
    const user = await MovilLibs().listId(Imei)
    const list = await RouteLibs().listTodayId(Imei, newDate, finishDate)
    const fullDataUSer = {
        Imei: user[0].Imei,
        Version_App: user[0].Version_App,
        CurrentModuloApp: user[0].CurrentModuloApp,
        Usuario: user[0].Usuario,
        Usuario_Id: user[0].Usuario_Id,
        EstadoUsuario: user[0].EstadoUsuario,
        Total_Cables_Pend: user[0].Total_Cables_Pend,
        Total_Equipos_Pend: user[0].Total_Equipos_Pend,
        Total_Fotos_Pend: user[0].Total_Fotos_Pend,
        Total_Novedades_Pend: user[0].Total_Novedades_Pend,
        Total_Postes_Pend: user[0].Total_Postes_Pend,
        GpsEnabled: user[0].GpsEnabled,
        OpenAplicacion: user[0].OpenAplicacion,
        Date_update: user[0].date_update,
        Route: list
    }
    return fullDataUSer
}

async function selectAllUserDay(initial, finish) {
    const newDate = new Date(initial)
    const finishDate = new Date(finish)
    const dataAllUSer = await MovilLibs().listAll()
    let newData = []
    for (let user of dataAllUSer) {
        const dataLastRoute = await RouteLibs().listTodayId(user.Imei, newDate, finishDate)
        newData.push({
            Imei: user.Imei,
            Version_App: user.Version_App,
            CurrentModuloApp: user.CurrentModuloApp,
            Usuario: user.Usuario,
            Usuario_Id: user.Usuario_Id,
            EstadoUsuario: user.EstadoUsuario,
            Longitud: dataLastRoute[0] ? dataLastRoute[0].Longitud : null,
            Latitud: dataLastRoute[0] ? dataLastRoute[0].Latitud : null,
            DireccionAproximada: dataLastRoute[0] ? dataLastRoute[0].DireccionAproximada : null,
            Ubicacion: dataLastRoute[0] ? dataLastRoute[0].LongUbicacionitud : null,
            Date_creation: dataLastRoute[0] ? dataLastRoute[0].Longitdate_creationud : null
        })
    }
    return newData
}

/* ok */
async function listLastRouteAllUser() {
    const dataAllUSer = await MovilLibs().listAll()
    let newData = []
    for (let user of dataAllUSer) {
        const dataLastRoute = await RouteLibs().listLast(user.Imei)
        newData.push({
            Imei: user.Imei,
            Version_App: user.Version_App,
            CurrentModuloApp: user.CurrentModuloApp,
            Usuario: user.Usuario,
            Usuario_Id: user.Usuario_Id,
            EstadoUsuario: user.EstadoUsuario,
            Longitud: dataLastRoute[0] ? dataLastRoute[0].Longitud : null,
            Latitud: dataLastRoute[0] ? dataLastRoute[0].Latitud : null,
            DireccionAproximada: dataLastRoute[0] ? dataLastRoute[0].DireccionAproximada : null,
            Ubicacion: dataLastRoute[0] ? dataLastRoute[0].LongUbicacionitud : null,
            Date_creation: dataLastRoute[0] ? dataLastRoute[0].Longitdate_creationud : null
        })
    }
    return newData
}

module.exports = {
    saveRoute,
    listLastRouteAllUser,
    listRouteUser,
    newConectUser,
    newDisconect,
    selectAllUserDay
}