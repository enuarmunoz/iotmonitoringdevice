'use strict'
const DataBaseAPI = require('../controller/route')

/*------------ metodo para agregar a un usuario a una sala de chat, es el primer metodo que se tiene que ejecutar(MOVIL) ---------*/
function newSlave(socket, io) {
    return socket.on('newSlave', async (data) => {

        console.log("NEW CONECT: ",data);
        data['Socket_id'] = socket.id
        await DataBaseAPI.newConectUser(data, true)
        socket.join(data.Imei)
        io.to(data.Imei).emit("confirmNewConect");
        io.emit("NewUserConect", data) 
        
    });
}

/* -----------guardar la data de cada celular en base de datos (MOVIL)----------------- */
function savaData(socket, io){
    return socket.on('saveData', (data)=>{
        console.log("SAVE DATA: ",data);
        DataBaseAPI.saveRoute(data)
        io.emit("NewUserConect", data) 
    }) 
}


/*------------ vamos a crear un metodo que nos permita identificar cuando un usuario se ha desconectado-------*/

function DisconectSlave(socket, io){    
    return socket.on('disconnect', async ()=>{
        console.log("DISCONECT: ",socket.id);
        const user = await DataBaseAPI.newDisconect(socket.id, false)
        io.emit("confirmNewDisconect")
        io.emit("NewUserDisconect", {Imei: user, action: false})
    }) 
}

module.exports = {
    newSlave,
    savaData,
    DisconectSlave
}