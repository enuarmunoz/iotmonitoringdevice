'use strict'
require('./config/db')
const express = require("express");
const asyncify = require('express-asyncify')
const bodyParser = require('body-parser')
const http = require('http')
var https    =require('https');
const socketIO = require("socket.io");
const chalk = require('chalk')
const socketRout = require('./socket/index')
const url = require('./url/index')
var fs      =require('fs');
const port = 3000
const app = asyncify(express())


//Set HTTPS
var options= {
  key:fs.readFileSync('../../../cert/pruebas.interedes.com.co.key'),
  cert:fs.readFileSync('../../../cert/pruebas.interedes.com.co.crt')
};

const server  = https.createServer(options,app);
//const server = http.createServer(app)

//Socket
const io = socketIO(server);
app.use(bodyParser.json())
app.use('/v1', url)


io.sockets.on('connection', (socket)=>{
  socketRout.newSlave(socket, io)
  socketRout.savaData(socket, io)
  socketRout.DisconectSlave(socket, io)
});

app.get('/', function(req, res) {
  res.send("Hello World!");
});



// Add headers
//app.use(cors({credentials: false}));
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

//server.listen(port,'192.168.100.144', () =>{
server.listen(port, () =>{
  console.log(`${chalk.blue('Interedes-Socket:')}servidor Corriendo en el puerto ${port}`)
})
