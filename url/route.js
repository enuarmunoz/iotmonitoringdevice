const express = require('express')
const asyncify = require('express-asyncify')
const serviceRoute = require('../controller/route')
 
const url =  asyncify(express.Router())

/* OK */
url.post("/newConect", async (request, response)=>{
    const dataNewConect = request.body
    const newConect = await serviceRoute.newConectUser(dataNewConect, true)
    response.json(newConect) 
})

/* OK */
url.post("/newDisconect", async (request, response)=>{
    const Imei = request.body.Imei
    const newConect = await serviceRoute.newDisconect(Imei ,false)
    response.json(newConect) 
})

/* ok */
url.post('/saveRoute', async (request, response) =>{
    const dataRoute = request.body
    const list = await serviceRoute.saveRoute(dataRoute)
    response.json(list)
})


/* OK  */
url.get('/selectRouteUser/:Imei/:initDate/:finishDate', async (request, response)=>{
    const Imei = request.params.Imei
    const init = request.params.initDate
    const finish = request.params.finishDate
    const list = await serviceRoute.listRouteUser(Imei, init, finish)
    response.json(list)
})

/* time  */
url.get('/selectAllUserDay/:initDate/:finishDate', async (request, response)=>{
    const init = request.params.initDate
    const finish = request.params.finishDate
    const list = await serviceRoute.selectAllUserDay(init, finish)
    response.json(list)
})

/* ok */
url.get('/selectLastRouteAllUser', async (request, response)=>{
    const list = await serviceRoute.listLastRouteAllUser()
    response.json(list)
})

module.exports = url  