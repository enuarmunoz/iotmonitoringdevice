'use strict'
const express = require('express')
const asyncify = require('express-asyncify')
const movil = require('./movil')
const router = require('./route')

const url = asyncify(express.Router())

url.use('/movil', movil)
url.use('/router', router)

url.get('/', function(req, res) { 
    res.send("Hello World!");
 });

module.exports = url