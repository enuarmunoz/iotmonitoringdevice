const express = require('express')
const asyncify = require('express-asyncify')
const serviceMovile = require('../controller/movil')
 
const url =  asyncify(express.Router())

url.post('/create', async (request, response) =>{
    const data = request.body
    const save = await serviceMovile.create(data)
    response.json(save)
})

url.get('/select/:id', async (request, response) =>{
    const id = request.params.id
    const list = await serviceMovile.list(id)
    response.json(list)
})

url.post('/update/:id', async (request, response) =>{
    const data = request.body
    const id = request.params.id
    const update = await serviceMovile.modify(id,data)
    response.json(update)
})

url.get('/selectAll', async (request, response) =>{
    const list = await serviceMovile.listAll()
    response.json(list)
})

module.exports = url  