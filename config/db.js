'use strict'
const chalk = require('chalk')
const mongoose = require('mongoose')
const { DATABASES } = require('./settings')

mongoose.connect(`mongodb://${DATABASES.default.USER}:${DATABASES.default.PASSWORD}@${DATABASES.default.HOST}/${DATABASES.default.NAME}?authMechanism=SCRAM-SHA-1`, { useNewUrlParser: true })
var db = mongoose.connection
db.on('error', function (error) {
  console.error(`${chalk.red('MongoDB connection error:')}`, error.message)
  process.exit(0)
})

 