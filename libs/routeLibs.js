'use strict'

const Route = require('../models/Route')
const makeErrors = require('../libs/util/makeErrors')

module.exports = function () {

    /* --------OK-------- */
    async function create(route) {
        const newRoute = Route(route)
        let response
        try {
            response = await newRoute.save()
        } catch (error) {
            response = makeErrors(error.errors)
        }
        return response
    }

    async function listAll() {
        try {
            const DataMovil = await Route.find()
            return DataMovil
        }
        catch (error) {
            return {
                'error': {
                    'veryfy': {
                        'message': 'No se encontraron usuarios en la base de datos o hay problemas en la conexion'
                    }
                }
            }
        }
    }

    /*----- ok - */
    async function listLast(id) {
        try {
            const list = await Route.find({ Imei: id }).sort({ date_creation: 'desc' }).limit(1)
            return list
        }
        catch (error) {
            return {
                'error': {
                    'veryfy': {
                        'message': 'No se encontro la ubicacion en la base de datos o hay problemas en la conexion'
                    }
                }
            }
        }
    }


    /*----- OK- */
    async function listTodayId(Imei, initialDate, finishDate) {
        try {
            const route = await Route.find({ Imei, Latitud:{ $ne:0 },Longitud:{ $ne:0 }, date_creation:{"$gte": initialDate, "$lt":finishDate}})
            return route
        }
        catch (error) {
            return {
                'error': {
                    'veryfy': {
                        'message': 'No se encontraron una ruta en la base de datos o hay problemas en la conexion'
                    }
                }
            }
        }
    }
    return {
        create,
        listAll,
        listTodayId,
        listLast,
    }

}