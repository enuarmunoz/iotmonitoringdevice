'use strict'

const Movil = require('../models/Movil')
const makeErrors = require('../libs/util/makeErrors')

module.exports = function () {

    async function create(movil) {
        const newMovil = Movil(movil)
        let response
        try {
            response = await newMovil.save()
        } catch (error) {
            response = makeErrors(error.errors)
        }
        return response
    }

    async function listAll() {
        try {
            const DataMovil = await Movil.find()
            return DataMovil
        }
        catch (error) {
            return {
                'error': {
                    'veryfy': {
                        'message': 'No se encontraron usuarios en la base de datos o hay problemas en la conexion'
                    }
                }
            }
        }
    }

    async function listId(id) {
        try {
            const DataMovil = await Movil.find({
                Imei: id
            })
            return DataMovil
        }
        catch (error) {
            return {
                'error': {
                    'veryfy': {
                        'message': 'No se encontraron usuarios en la base de datos o hay problemas en la conexion'
                    }
                }
            }
        }
    }

    async function UpdateEstado(Imei, DataUpdate) {
        DataUpdate['date_update'] = Date.now()
        try {
            const response = await Movil.updateOne({ Imei }, DataUpdate, { runValidators: true })
            return response
        }
        catch (error) {
        }
    }

    async function update(id, fieldData) {
        fieldData['date_update'] = Date.now()
        try {
            const response = await Movil.updateOne({ Imei: id }, fieldData, { runValidators: true })
            return response
        }
        catch (error) {
            return makeErrors(error.errors)
        }
    }

    async function listByIdSocket(id){
        try {
            const MovilData = await Movil.find({ Socket_id: id})
            return MovilData
        }
        catch (error) {
            return {
                'error': {
                    'veryfy': {
                        'message': `No se encontraron una ruta en la base de datos o hay problemas en la conexion ${error}`
                    }
                }
            }
        }
    }

    return {
        create,
        listAll,
        update,
        listId,
        UpdateEstado,
        listByIdSocket
    }

}